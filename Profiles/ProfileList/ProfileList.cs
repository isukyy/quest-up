﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.IO;
using System.Xml;

namespace QuestUp.Profiles.ProfileList
{
    class ProfileList
    {
        private DirectoryInfo directoryInfo;
        private string profilesPath;

        private StackPanel stackPanel;
        
        /// <summary>
        /// This class handles the profile list
        /// </summary>
        /// <param name="StackPanel"></param>
        /// <param name="ProfilesPath"></param>
        public ProfileList(StackPanel StackPanel, string ProfilesPath)
        {
            this.profilesPath = ProfilesPath;
            this.stackPanel = StackPanel;

            directoryInfo = new DirectoryInfo(this.profilesPath);
        }


        /// <summary>
        /// Creates Profilepages for each profile in the profile folder
        /// </summary>
        public void createProfilePages()
        {
            int xmlCount = 0;

            foreach (string xmlFilePath in
                     Directory.GetFiles(profilesPath, "*.xml"))
            {
                xmlCount++;

                XmlReader xmlReader = XmlReader.Create(xmlFilePath);

                string name, gender, level,
                       race, characterClass = String.Empty;

                using (XmlReader reader = XmlReader.Create(xmlFilePath))
                {
                    // Moves the reader to the root element.
                    reader.MoveToContent();

                    // Moves to book node.
                    reader.Read();

                    //Load character information from the Xml
                    name             = reader.ReadInnerXml();
                    gender           = reader.ReadInnerXml();
                    characterClass   = reader.ReadInnerXml();
                    race             = reader.ReadInnerXml();
                    level            = reader.ReadInnerXml();

                    //Closes the xmlReader and disposes it
                    reader.Close();
                    reader.Dispose();

                }

                //Creates a profilepage for a profile
                ProfilePage profPage = new ProfilePage();
                profPage.Height = 142;
                profPage.Width = 1280;
                profPage.VerticalAlignment = VerticalAlignment.Top;
                profPage.BorderBrush = new SolidColorBrush(Colors.White);
                profPage.BorderThickness = new Thickness(2);
                profPage.label_name.Content   = name;
                profPage.label_class.Content  = characterClass;
                profPage.label_gender.Content = gender;
                profPage.label_race.Content   = race;
                profPage.label_level.Content   = level;
                profPage.Margin = new Thickness(0, 10 , 2, 0);

                //Adds the profile page to the main stackpanel
                stackPanel.Children.Insert(0, profPage);
            }

        }
    }

}

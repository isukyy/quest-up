﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestUp.Profiles
{
    class ProfileInformation
    {
        /// <summary>
        /// Gets the selected gender
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string getGenderAsString(int index)
        {
            string gender = String.Empty;

            switch(index)
            {
                case 0:
                    gender = "Male";
                    break;
                case 1:
                    gender = "Female";
                    break;
            }

            return gender;
        }

        /// <summary>
        /// Gets the selected Class
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static string getClassAsString(int index)
        {
            string _class = String.Empty;

            switch (index)
            {
                case 0:
                    _class = "Knight";
                    break;
                case 1:
                    _class = "Archer";
                    break;
                case 2:
                    _class = "Mage";
                    break;
            }

            return _class;
        }

        public static string getRaceAsString(int index)
        {
            string race = String.Empty;

            switch(index)
            {
                case 0:
                    race = "Human";
                    break;
                case 1:
                    race = "Elf";
                    break;
                case 2:
                    race = "Feline Fox";
                    break;
                case 3:
                    race = "Feline Wolf";
                    break;
                case 4:
                    race = "Anthro Fox";
                    break;
                case 5:
                    race = "Anthro Wolf";
                    break;
            }

            return race;
        }



    }
}

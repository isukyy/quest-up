﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuestUp.Profiles.Profile
{
    class Profile
    {
        string sname, sclass, srace, slvl;
        int iclass, irace, ilvl;

        public Profile(string Name, string Class, string Race, string Lvl)
        {
            this.sname = Name;
            this.sclass = Class;
            this.srace = Race;
            this.slvl = Lvl;
        }

        public Profile(string Name, int Class, int Race, int Lvl)
        {
            this.sname = Name;
            this.iclass = Class;
            this.irace = Race;
            this.ilvl = Lvl;
        }
    }
}

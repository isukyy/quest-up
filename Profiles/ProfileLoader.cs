﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace QuestUp
{
    class Profile
    {
        public static ProfileCreator profileCreator;

        /// <summary>
        /// Creates a Profile
        /// </summary>
        public static void Create()
        {
            profileCreator = new ProfileCreator();
            profileCreator.Topmost = true;
            profileCreator.Show();
        }

        /// <summary>
        /// Checks if there are Profiles
        /// </summary>
        public static void checkProfileList()
        {
            //Check if the roaming folder exists
            if (!Directory.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles"))
            {
                System.IO.Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles");
            } 

            else
            {
                string[] profiles = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles");
                
                if(profiles.Count() == 0)
                {
                    Create();
                }
            }
        }
    }
}

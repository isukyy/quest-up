﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using QuestUp.Profiles.ProfileList;

namespace QuestUp
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public static bool profileExists = false;

        public List <Control>useableControls;

        private ProfileList profileList;


        public MainWindow()
        {
            InitializeComponent();

            //creates ProfileList
            profileList = new ProfileList(mainStackPanel, Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles/");

            //Checks if profiles are existing
            if (Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles/", "*.xml", SearchOption.AllDirectories).Length > 0)
            {
                profileExists = true;

                profileList.createProfilePages();

                LabelNoProfiles.Visibility = Visibility.Hidden;
            }

            //If there are no profiles it opens the profilecreation
            else
            {
                LabelNoProfiles.Visibility = Visibility.Visible;
                Profile.checkProfileList();
            }

        }

        /*
         * Minimize the Window
         */
        private void WindowHideButton_Click(object sender, RoutedEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        /*
         * Drags the Window when Mouse_1 is pressed
         */
        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WindowState = System.Windows.WindowState.Normal;
            DragMove();

            if (e.ClickCount == 2)
                WindowState = System.Windows.WindowState.Maximized;
        }

        public void disableNoProfilesWarning()
        {
            LabelNoProfiles.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowCloseButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (Profile.profileCreator != null)
                {
                    Profile.profileCreator.Close();
                    Close();
                }

                Close();
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        /// <summary>
        /// Maximizes the window or when it´s maximized it brings back the normal size
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowMaximizeButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (WindowState != System.Windows.WindowState.Maximized)
            {
                WindowState = System.Windows.WindowState.Maximized;
            }

            else
            {
                WindowState = System.Windows.WindowState.Normal;
            }
        }

        /// <summary>
        /// Minimizes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WindowHideButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WindowState = System.Windows.WindowState.Minimized;
        }

        /// <summary>
        /// Opens the Profile Creator
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_createNewProfile_click(object sender, RoutedEventArgs e)
        {
            ProfileCreator profCreator = new ProfileCreator();

            profCreator.Activate();
            profCreator.Show();
        }

        /// <summary>
        /// If this button is clicked the profile list gets refreshed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_refresh_Click(object sender, RoutedEventArgs e)
        {
            refreshProfileListElements();
        }

        /// <summary>
        /// Refreshes the profile list
        /// </summary>
        public void refreshProfileListElements()
        {
            for (int element = mainStackPanel.Children.Count; element > 0; element--)
            {

                Console.WriteLine(element.ToString());
                mainStackPanel.Children.RemoveAt(element - 1);

            }

            profileList.createProfilePages();
        }
    }
}

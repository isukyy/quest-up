﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Xml;

namespace QuestUp
{
    /// <summary>
    /// Interaction logic for ProfileCreator.xaml
    /// </summary>
    public partial class ProfileCreator : Window
    {

        public ProfileCreator()
        {
            InitializeComponent();
            Console.WriteLine("" + genderCombo.SelectedIndex);   
        }

        private void Rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        /// <summary>
        /// Creates the profile
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCreate_Click(object sender, RoutedEventArgs e)
        {

            if(nameBox.Text != null && genderCombo.SelectedIndex != -1 && classCombo.SelectedIndex != -1 && raceCombo.SelectedIndex != -1)
            {

                var mainWin = Application.Current.Windows
                                .Cast<Window>()
                                .FirstOrDefault(window => window is MainWindow) as MainWindow;

                 mainWin.LabelNoProfiles.Visibility = Visibility.Hidden;

                   if (!System.IO.File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles/" + nameBox.Text + ".xml"))
                     {
                         var myFile = File.Create(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles/" + nameBox.Text + ".xml");
                         myFile.Close();  

                         if (File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/QuestUp/Profiles/" + nameBox.Text + ".xml"))
                         {

                                // TODO add support for Windows 2000 and Windows XP (AppData = Application Data)
                                XmlWriter xmlWriter = XmlWriter.Create(Environment.GetEnvironmentVariable("userprofile") + "/" + "AppData" +"/Roaming"+ "/QuestUp/Profiles/" + nameBox.Text + ".xml");

                                xmlWriter.WriteStartDocument();

                                xmlWriter.WriteStartElement("Profile");
                                xmlWriter.WriteStartElement("Name");
                                xmlWriter.WriteString(nameBox.Text);
                                xmlWriter.WriteEndElement();
                             
                                xmlWriter.WriteStartElement("Gender");
                                xmlWriter.WriteString(Profiles.ProfileInformation.getGenderAsString(genderCombo.SelectedIndex));
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("Class");
                                xmlWriter.WriteString(Profiles.ProfileInformation.getClassAsString(classCombo.SelectedIndex));
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("Race");
                                xmlWriter.WriteString(Profiles.ProfileInformation.getRaceAsString(raceCombo.SelectedIndex));
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("Lvl");
                                xmlWriter.WriteString("1");
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteStartElement("Exp");
                                xmlWriter.WriteString("0");
                                xmlWriter.WriteEndElement();

                                xmlWriter.WriteEndDocument();
                                xmlWriter.Close();

                                mainWin.refreshProfileListElements();

                                this.Close();

                             // TODO Show gameinterface after profile creation
                         }
                     }

                     else
                     {
                         MessageBox.Show("Name exestiert bereits!");
                     }
                       
            }

            else
            {
                MessageBox.Show("Alle Felder müssen ausgewählt oder ausgefüllt sein!");
            }
        }



        /// <summary>
        /// Closes the window
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonClose_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var mainWin = Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) as MainWindow;

            this.Close();
        }

        /// <summary>
        /// Do stuff when window was activated
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Window_Activated(object sender, EventArgs e)
        {
            var mainWin = Application.Current.Windows
                .Cast<Window>()
                .FirstOrDefault(window => window is MainWindow) as MainWindow;
        }
    }
}
